$(document).ready(function () {
    let path = $('#search-authors-path').val();
    $('.select').select2({
        allowClear: true,
        minimumInputLength: 1,
        placeholder: 'Enter the first letters of the book',
        ajax: {
            method: 'POST',
            url: path,
            delay: 300,
            data: function (params) {
                let param = '';
                if (params.term) {
                    param = params.term
                }
                return {
                    search: param,
                };
            },
            dataType: 'json',
            processResults: function (response) {
                let result = [];
                if (response) {
                    $.each(response.authors, function (i, author) {
                        let item;
                        item = {
                            "id": author.id,
                            "text": author.fullName,
                        };
                        result.push(item);
                    });
                }

                return {
                    results: result,
                };
            }
        }
    });
});