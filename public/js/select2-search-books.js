$(document).ready(function () {
    let path = $('#search-books-path').val();

    let select = $('.select').select2({
        allowClear: true,
        minimumInputLength: 1,
        placeholder: 'Enter the first letters of the book',
        ajax: {
            method: 'POST',
            url: path,
            delay: 300,
            pagination: {
                more: true
            },
            data: function (params) {
                console.log(params)
                let param = '';
                if (params.term) {
                    param = params.term
                }
                return {
                    search: param,
                    page: params.page || 1
                };
            },
            dataType: 'json',
            processResults: function (response) {
                let result = [];
                if (response) {
                    $.each(response.books, function (i, book) {
                        let item;
                        item = {
                            "id": book.id,
                            "text": book.title,
                        };
                        result.push(item);
                    });
                }

                return {
                    results: result,
                };
            }
        }
    });

    // if ($('#get-books-path').length) {
    //     let path = $('#get-books-path').val();
    //
    //     $.ajax({
    //         url: path,
    //         method: "GET",
    //         success: function (response) {
    //             if (response) {
    //                 $.each(response.books, function (i, book) {
    //                     select.append(new Option(book.title, book.id, true, true)).trigger('change');
    //                 });
    //             }
    //
    //             if (response.pages > 1) {
    //                 for (i = 1; i <= response.pages; i++) {
    //                     $.ajax({
    //                         url: path + '?page=' + i,
    //                         method: "GET",
    //                         // async: false,
    //                         success: function (response) {
    //                             if (response) {
    //                                 $.each(response.books, function (i, book) {
    //                                     select.append(new Option(book.title, book.id, true, true)).trigger('change');
    //                                 });
    //                             }
    //                         }
    //                     });
    //                 }
    //             }
    //         }
    //     });
    // }
});