<?php

namespace App\Controller;

use App\Entity\Book;
use App\Repository\AuthorRepository;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/api/v1")
 */
class ApiController extends AbstractFOSRestController implements HeaderAuthController
{
    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @Rest\Get("/books/list")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param BookRepository $bookRepository
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getBookLists(Request $request, PaginatorInterface $paginator, BookRepository $bookRepository)
    {
        $books = $paginator->paginate(
            $bookRepository->findAllQuery(),
            $request->query->getInt('page', 1),
            100);

        return $this->json([
            'pages' => ceil($books->getTotalItemCount() / 100),
            'books' => $books
        ],
            Response::HTTP_OK,
            [], [
                ObjectNormalizer::ENABLE_MAX_DEPTH => true,
                ObjectNormalizer::IGNORED_ATTRIBUTES => ['books', 'choiceBooks'],
            ]);
    }

    /**
     * @Rest\Get("/books/{id}")
     * @param BookRepository $bookRepository
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getBook(BookRepository $bookRepository, int $id)
    {
        $book = $bookRepository->find($id);
        return $this->json($book, Response::HTTP_OK, [], [
            ObjectNormalizer::ENABLE_MAX_DEPTH => true,
            ObjectNormalizer::IGNORED_ATTRIBUTES => ['books', 'choiceBooks'],
        ]);
    }

    /**
     * @Rest\Post("/books/{id}")
     * @param Request $request
     * @param Book $book
     * @param AuthorRepository $authorRepository
     * @param ValidatorInterface $validator
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function updateBook(Request $request, Book $book, AuthorRepository $authorRepository, ValidatorInterface $validator)
    {
        $data = json_decode($request->getContent(), true);
        $book->setTitle($data['title']);
        if (isset($data['authors']) && !empty($data['authors'])) {
            foreach ($data['authors'] as $authorId) {
                $author = $authorRepository->find($authorId);
                if ($author) {
                    $book->addAuthor($author);
                }
            }
        }
        $errors = $validator->validate($book);
        if (count($errors)) {
            $messages = [];
            foreach ($errors as $error) {
                $messages[$error->getPropertyPath()] = $error->getMessage();
            }
            return $this->json(['message' => $messages], Response::HTTP_BAD_REQUEST);
        }
        $this->em->flush();

        return $this->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @Rest\Delete("/books/{id}")
     * @param Book $book
     * @return View
     */
    public function deleteBook(Book $book)
    {
        $this->em->remove($book);
        $this->em->flush();

        return $this->view([], Response::HTTP_NO_CONTENT);
    }
}