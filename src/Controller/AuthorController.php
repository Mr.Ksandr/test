<?php

namespace App\Controller;

use App\Entity\Author;
use App\Entity\Book;
use App\Form\AuthorType;
use App\Repository\AuthorRepository;
use App\Repository\BookRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/author")
 */
class AuthorController extends AbstractController
{
    /**
     * @Route("/", name="author_index", methods={"GET"})
     * @param AuthorRepository $authorRepository
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(AuthorRepository $authorRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $pagination = $paginator->paginate(
            $authorRepository->findAllQuery(),
            $request->query->getInt('page', 1),
            15);

        return $this->render('author/index.html.twig', [
            'authors' => $pagination,
        ]);
    }

    /**
     * @Route("/", name="books_search", methods={"POST"})
     * @param Request $request
     * @param BookRepository $bookRepository
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function searchBooks(Request $request, BookRepository $bookRepository)
    {
        $books = $bookRepository->findBySearchWord($request->request->get('search'));

        return $this->json(['books' => $books]);
    }

    /**
     * @Route("/books/{authorId}", name="books_all", methods={"GET"})
     * @param Request $request
     * @param int $authorId
     * @param BookRepository $bookRepository
     * @param PaginatorInterface $paginator
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getBooks(Request $request, int $authorId, BookRepository $bookRepository, PaginatorInterface $paginator)
    {
        $pagination = $paginator->paginate(
            $bookRepository->findByAuthorIdQuery($authorId),
            $request->query->getInt('page', 1),
            100);

        return $this->json([
            'books' => $pagination->getItems(),
            'pages' => ceil($pagination->getTotalItemCount() / 100)
        ]);
    }

    /**
     * @Route("/new", name="author_new", methods={"GET","POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param BookRepository $bookRepository
     * @return Response
     */
    public function new(Request $request, ValidatorInterface $validator, BookRepository $bookRepository): Response
    {
        $author = new Author();
        $form = $this->createForm(AuthorType::class, $author);
        $form->handleRequest($request);

        if ($form->isSubmitted() && !count($validator->validate($author))) {
            $data = $request->request->get('author');
            if (isset($data['choiceBooks']) && !empty($data['choiceBooks'])) {
                $booksIds = $data['choiceBooks'];
                foreach ($booksIds as $id) {
                    $book = $bookRepository->find($id);
                    if ($book) {
                        $author->addBook($book);
                    }
                }
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($author);
            $entityManager->flush();

            return $this->redirectToRoute('author_index');
        }

        return $this->render('author/new.html.twig', [
            'author' => $author,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="author_show", methods={"GET"})
     * @param Request $request
     * @param int $id
     * @param AuthorRepository $authorRepository
     * @param BookRepository $bookRepository
     * @param PaginatorInterface $paginator
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function show(int $id, AuthorRepository $authorRepository): Response
    {
        $author = $authorRepository->findById($id);

        return $this->render('author/show.html.twig', [
            'author' => $author,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="author_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Author $author
     * @param AuthorRepository $authorRepository
     * @param BookRepository $bookRepository
     * @param PaginatorInterface $paginator
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function edit(
        Request $request,
        Author $author,
        AuthorRepository $authorRepository,
        BookRepository $bookRepository,
        PaginatorInterface $paginator,
        ValidatorInterface $validator
    ): Response
    {
        $form = $this->createForm(AuthorType::class, $author);
        $form->handleRequest($request);
        if ($form->isSubmitted() && !count($validator->validate($author))) {
            $data = $request->request->get('author');
            if (isset($data['choiceBooks']) && !empty($data['choiceBooks'])) {
                $booksIds = $data['choiceBooks'];
                foreach ($booksIds as $id) {
                    $book = $bookRepository->find($id);
                    if ($book) {
                        $author->addBook($book);
                    }
                }
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('author_index');
        }
        $books = $paginator->paginate(
            $bookRepository->findByAuthorIdQuery($author->getId()),
            $request->query->getInt('page', 1),
            15);

        return $this->render('author/edit.html.twig', [
            'author' => $author,
            'form' => $form->createView(),
            'books' => $books
        ]);
    }

    /**
     * @Route("/{id}", name="author_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Author $author): Response
    {
        if ($this->isCsrfTokenValid('delete' . $author->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($author);
            $entityManager->flush();
        }

        return $this->redirectToRoute('author_index');
    }

    /**
     * @Route("/book/{id}/{bookId}", name="remove_book", methods={"DELETE"})
     * @param Request $request
     * @param Author $author
     * @param int $bookId
     * @param BookRepository $bookRepository
     * @return Response
     */
    public function removeBook(Request $request, Author $author, int $bookId, BookRepository $bookRepository): Response
    {
        $book = $bookRepository->find($bookId);
        if ($request->isXmlHttpRequest() && $book && $author) {
            $author->removeBook($book);
            $this->getDoctrine()->getManager()->flush();

            return $this->json(['ok']);
        }

        return $this->json(['message' => 'Error data'],400);
    }
}
