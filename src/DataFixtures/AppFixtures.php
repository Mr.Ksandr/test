<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->encoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $authors = [];
        $faker = Factory::create();

        $user = new User();
        $user->setUsername('admin')
            ->setPassword($this->encoder->encodePassword($user, '0000'))
            ->setRoles(['ROLE_ADMIN']);

        $manager->persist($user);

        for ($i = 0; $i <= 2; $i++) {
            $author = (new Author())->setFullName($faker->firstName . ' ' . $faker->lastName);
            $manager->persist($author);
            $authors[] = $author;
        }
        for ($i = 0; $i <= 100000; $i++) {
            $author = (new Book())->setTitle($faker->text(20))->addAuthor($authors[0]);
            $manager->persist($author);
        }
        $manager->flush();
    }
}
