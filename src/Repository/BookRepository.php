<?php

namespace App\Repository;

use App\Entity\Author;
use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function findAllQuery()
    {
        return $this->createQueryBuilder('b')
            ->orderBy('b.id', 'DESC')
            ->getQuery();
    }

    public function findBySearchWord(string $word)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.title LIKE :word')
            ->setParameter('word', "%$word%")
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @param int $id
     * @return \Doctrine\ORM\Query
     */
    public function findByAuthorIdQuery(int $id)
    {
        return $this->createQueryBuilder('b')
            ->select('b.id','b.title')
            ->leftJoin('b.authors', 'a')
            ->andWhere('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery();
    }
}
