<?php

namespace App\Repository;

use App\Entity\Author;
use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Author|null find($id, $lockMode = null, $lockVersion = null)
 * @method Author|null findOneBy(array $criteria, array $orderBy = null)
 * @method Author[]    findAll()
 * @method Author[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Author::class);
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function findAllQuery()
    {
        return $this->createQueryBuilder('a')
            ->select('a.id', 'a.fullName', 'COUNT(b) AS books')
            ->leftJoin('a.books', 'b')
            ->orderBy('a.id', 'DESC')
            ->groupBy('a.id')
            ->getQuery();
    }

    /**
     * @param int $id
     * @return \Doctrine\ORM\Query
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findById(int $id)
    {
        return $this->createQueryBuilder('a')
            ->select('a.id', 'a.fullName', 'COUNT(b) AS books')
            ->leftJoin('a.books', 'b')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $word
     * @return Author[] Returns an array of Author objects
     */
    public function findBySearchWord(string $word)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.fullName LIKE :word')
            ->setParameter('word', "%$word%")
            ->getQuery()
            ->getArrayResult();
    }

}
